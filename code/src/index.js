'use strict';
var Alexa = require('alexa-sdk');
var APP_ID = undefined;  // TODO replace with your app ID (OPTIONAL).


/* COPY AND PASTE YOUR FACTS BELOW THIS LINE */
//Blindness
var languageStrings = {
    "en-US": {
        "translation": {
            "FACTS": [
                "Japanese beer cans have alcohol written in braille on the top of the can to prevent blind people from accidently buying beer.",
                "No one born blind has ever developed schizophrenia.",
                "Blind people can get issued a guide horse. Miniature horses are used instead of guide dogs if the person is, for example, afraid of or allergic to, dogs.",
                "Red color blind monkeys who underwent gene therapy by having their eyes injected with a red cone, had their color blindness cured, potentially opening a door for curing color blindness in humans.",
                "There exist people who can locate objects through echolocation, making it possible for blind people to ride bicycles through traffic. A blind American man named Daniel Kish taught himself to navigate using echolocation. His hobbies include hiking and mountain-biking, and he has now trained over 500 students in his tongue click technique.",
                "Blind people can smile even though they have never seen anybody else smile.",
                "Mr. Rogers made a point of mentioning out loud when he was feeding his fish after he got a letter from a family whose blind daughter asked him to do so, because she couldn’t tell if the fish were being fed.",
                "Blind people have four times more nightmares than sighted people.",
                "Brian Jacques wrote the Redwall series for the children at the Royal Wavertree School for the Blind in Liverpool, where as a truck driver, he delivered milk. This is the reason for his very descriptive writing style.",
                "One time, the US Army used color blind people to spot camouflage colors that fooled those with normal color vision.",
                "The US Government has rigorous specs for the pens it buys. It has to be assembled by blind people. The pens must be able to write for a mile, work in temperatures 160 to negative 40 degrees fahrenheit, and are designed to fit undetected into uniforms. They should be able to stand in for a two inch fuse, and should be able to be used for an emergency tracheotomy.",
                "After making 1.4 billion crayons, the senior crayon maker for Crayola admitted that he was color blind.",
                "Braille was originally created as a way for Napoleon’s spies to read documents without needing any light. It was Louis Braille that pointed out the flaws of the code which created the version we use today.",
                "There are Playboy magazines printed in braille. In fact, the American Council of the Blind successfully sued the Library of Congress to make Playboy available in Braille.",
                "Guide dogs for the visually impaired are taught to go to the toilet on command, so that their owners are able to clean it up.",
                "Blind hiker Bill Irwin completed the Appalachian Trail with the help of his seeing eye dog, after falling about 5,000 times.",
                "Blind people experience visual hallucinations when they take LSD.",
                "A 15-year-old blind kid was able to use his enhanced hearing to hack telephone systems by perfectly reproducing dial tones. He even went as far as faking calls to the SWAT team in order to have them surround the houses of his enemies.",
                "J.D. Salinger never relinquished rights for audiobooks. The only legal way to obtain a Catcher in the Rye audiobook, is to have permission from a physician or ophthalmologist to listen to a legal audiobook recorded for the Library of Congress, who can do so by law, but only for the blind.",
                "The blind get a 50% discount on TV licenses in the UK.",
                "There is a restaurant in Toronto, Canada called O.Noir where the servers are blind and you dine in complete pitch black darkness.",
                "In Japan, they have specific sections of sidewalks textured for blind people’s canes so that they know where shops, subways, and crosswalks are located.",
                "Two guide dogs, Salty and Roselle, were awarded the Dickin Medal for gallantry after leading their blind owners out of the World Trade Center during the September 11 attacks.",
                "The explosion from the first nuclear weapon test at Trinity was so bright that Georgia Green, a blind woman almost 50 miles away, asked her brother, What’s that light?",
                "Theodore Roosevelt became blind in one eye from an injury suffered during a boxing match, while he was still in the office.",
                "In 2015 there were 61,739 blind children enrolled in school in the United States.",
                "For working age adults reporting significant vision loss, only 40.4% were employed in 2014.",
                "There are approximately 10,000 guide dog teams currently working in the United States.",
                "Only about 2 percent of all people who are blind and visually impaired work with guide dogs."
            ],
            "SKILL_NAME" : "Insert Name Here",
            "GET_FACT_MESSAGE" : ["Here's your fact: ", "Sure! Your fact is: ", "Did you know: ", "Sure. ", "Here's a fact: "],
            "HELP_MESSAGE" : "You can say tell me a XXXXXX fact, or, you can say exit... What can I help you with?",
            "HELP_REPROMPT" : "What can I help you with?",
            "STOP_MESSAGE" : "Goodbye!",
            "SOURCE" : "This information was obtained from the kickassfacts.com website and the National Federation of the Blind."
        }
    }
};
/* COPY AND PASTE YOUR FACTS ABOVE THIS LINE */


/*
This function is what Amazon Lambda uses to pass the call into the code for acting upon. Some of the parameters described below:
event – AWS Lambda uses this parameter to pass in event data to the handler.
context – AWS Lambda uses this parameter to provide your handler the runtime information of the Lambda function that is executing.
callback – You can use the optional callback to return information to the caller, otherwise return value is null.
handler – This is the name of the function AWS Lambda invokes. You export this so it is visible to AWS Lambda. The file name is index.js. So, index.handler is the handler. 
*/
exports.handler = function(event, context, callback) {
    var alexa = Alexa.handler(event, context);
    alexa.APP_ID = APP_ID;
    // To enable string internationalization (i18n) features, set a resources object.
    // For this demo, we are only using English. 
    // languageStrings - refers to the variable above that holds the facts. 
    alexa.resources = languageStrings;

    // You can have multiple handlers passed to registerHandlers, but for this example,
    // we only have one handler for this Alexa skill. 
    alexa.registerHandlers(handlers);

    // The execute command will 
    alexa.execute();
};

var handlers = {
    'LaunchRequest': function () {
        this.emit('GetFact');
    },
    'GetNewFactIntent': function () {
        this.emit('GetFact');
    },
    'GetFact': function () {
        // Get a random space fact from the space facts list
        // Use this.t() to get corresponding language data
        var factArr = this.t('FACTS');
        var factIndex = Math.floor(Math.random() * factArr.length);
        var randomFact = factArr[factIndex];

        // Get a random intro to the message
        var factMsgArr = this.t('GET_FACT_MESSAGE');
        factIndex = Math.floor(Math.random() * factMsgArr.length);
        var randomMsgFact = factMsgArr[factIndex];

        // Create speech output
        var speechOutput = randomMsgFact + randomFact;
        var cardOutput = randomFact + " " + this.t('SOURCE');
        this.emit(':tellWithCard', speechOutput, this.t("SKILL_NAME"), cardOutput);
    },
    'AMAZON.HelpIntent': function () {
        var speechOutput = this.t("HELP_MESSAGE");
        var reprompt = this.t("HELP_MESSAGE");
        this.emit(':ask', speechOutput, reprompt);
    },
    'AMAZON.CancelIntent': function () {
        this.emit(':tell', this.t("STOP_MESSAGE"));
    },
    'AMAZON.StopIntent': function () {
        this.emit(':tell', this.t("STOP_MESSAGE"));
    }
};